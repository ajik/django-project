from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('article', views.getArticles, name='get all articles'),
    path('article/insert', views.insertArticle, name='insert article')
]