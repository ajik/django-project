from django.db import models

# Create your models here.
class ArticleManager(models.Manager):
    def create_article(self, title, content, create_at, update_at):
        article = self.create(title = title, content = content, create_at = create_at, update_at = update_at)
        return article

class Article(models.Model):
    title = models.CharField(max_length=100)
    create_at = models.DateTimeField()
    update_at = models.DateTimeField(null=True)
    content = models.TextField(null=True)

    objects = ArticleManager()

    class Meta:
        db_table = "article"

