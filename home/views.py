from django.http import HttpResponse
from django.shortcuts import render
from .models import Article
from datetime import datetime


def home(request):
    return render(request, 'template/home.html')

def insertArticle(request):
    try:
        article = Article.objects.create_article('title', 'content article bla bla bla',datetime.now() , datetime.now())
        return HttpResponse("success create")
    except Exception as e:
        return HttpResponse(e)

def getArticles(request):
    try:
        articles = Article.objects.all()
        context = {}
        context['articles'] = articles
        return render(request, "template/show_article.html", context)
    except Exception as e:
        return HttpResponse(e)
