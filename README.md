run project: 
<code>python manage.py runserver [port]</code>
<small>Note: please prevent port 8080!</small>

add new module/application:
<code>python manage.py startapp [app name] </code>

run with Bash:
- open with git bash (windows), then run these 2 commands below
- in linux, just open terminal and execute these 2 commands below
- actually can only with number 2 commands, if occured an error, run the first one, then run the second
1. <code>chmod +x runserver</code>
2. <code>./runserver</code> or <code>./runserver.sh</code>

Generate new table for database:
1. <code>python manage.py makemigrations [app-name]</code>
2. <code>python manage.py migrate</code>
